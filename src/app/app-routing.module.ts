import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './authentication/components/login-page/login-page.component';
import { RegisterPageComponent } from './authentication/components/register-page/register-page.component';
import { ProfilePageComponent } from './user-profile/components/profile-page/profile-page.component';
import { UserPageComponent } from './admin/components/user-page/user-page.component';
import { AuthGuardService } from './authentication/services/auth-guard.service';
import { AddressPageComponent } from './admin/components/address-page/address-page.component';
import { UserDetailsPageComponent } from './admin/components/user-details-page/user-details-page.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'users',
    pathMatch: 'full',
  },
  {
    path: 'profile',
    component: ProfilePageComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'users',
    component: UserPageComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'users/:id',
    component: UserDetailsPageComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'addresses',
    component: AddressPageComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: 'register',
    component: RegisterPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
