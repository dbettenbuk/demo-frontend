import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import {
  MatButtonModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatIconModule,
  MatMenuModule,
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatTableModule,
} from '@angular/material';
import { LinkComponent } from './components/link/link.component';
import { AppRoutingModule } from '../app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BasicDialogComponent } from './components/basic-dialog/basic-dialog.component';

const materialModules = [
  CommonModule,
  MatButtonModule,
  MatFormFieldModule,
  MatToolbarModule,
  MatIconModule,
  MatMenuModule,
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatTableModule,
];

@NgModule({
  declarations: [HeaderComponent, LinkComponent, BasicDialogComponent],
  imports: [
    ...materialModules,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  exports: [
    ...materialModules,
    HeaderComponent,
    LinkComponent,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BasicDialogComponent,
  ],
  entryComponents: [
    BasicDialogComponent,
  ]
})
export class SharedModule { }
