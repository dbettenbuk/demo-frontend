import { MonoTypeOperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';

export default function handleRefresh<T>(refreshFunction: () => void): MonoTypeOperatorFunction<T> {
  return input$ => input$.pipe(
    map(result => {
      refreshFunction();
      return result;
    }));
}
