import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface IRequestOptions {
  headers?: HttpHeaders | { [header: string]: string | string[]; };
  params?: HttpParams | { [param: string]: string | string[] };
  responseType?: 'json';
}

@Injectable()
export class ApiClient {
  constructor(private httpClient: HttpClient) { }

  public post<T>(endpoint: string, body?: any, options?: IRequestOptions): Observable<T> {
    return this.httpClient.post<T>(this.getUrl(endpoint), body, options);
  }

  public get<T>(endpoint: string, options?: IRequestOptions): Observable<T> {
    return this.httpClient.get<T>(this.getUrl(endpoint), options);
  }

  public delete<T>(endpoint: string, options?: IRequestOptions): Observable<T> {
    return this.httpClient.delete<T>(this.getUrl(endpoint), options);
  }

  public put<T>(endpoint: string, body?: any, options?: IRequestOptions): Observable<T> {
    return this.httpClient.put<T>(this.getUrl(endpoint), body, options);
  }

  private getUrl(endpoint: string) {
    return environment.apiUrl + endpoint;
  }
}
