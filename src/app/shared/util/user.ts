import { IAddress } from './address';
import { IUserRole } from './user-role';

export interface IUser {
  id?: string;
  email: string;
  roles?: IUserRole[];
  password?: string;
  address?: IAddress;
}
