export interface IAddress {
  id?: string;
  city: string;
  streetAddress: string;
}
