import { UserRoleType } from './user-role-type';

export interface IUserRole {
  id?: string;
  type: UserRoleType;
}
