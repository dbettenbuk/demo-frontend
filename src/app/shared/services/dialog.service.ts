import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { BasicDialogComponent } from '../components/basic-dialog/basic-dialog.component';
import { Observable, Subscription } from 'rxjs';

const DEFAULT_SUCCESS_DATA = {
  title: 'Success',
  content: 'Operation successful',
  secondButtonCaption: 'Ok',
};

const DEFAULT_ERROR_DATA = {
  title: 'Error',
  secondButtonCaption: 'Ok',
};

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private matDialog: MatDialog) { }

  public openSuccessDialog(): MatDialogRef<BasicDialogComponent> {
    return this.matDialog.open(BasicDialogComponent, { data: DEFAULT_SUCCESS_DATA });
  }

  public openErrorDialog(error: any): MatDialogRef<BasicDialogComponent> {
    return this.matDialog.open(BasicDialogComponent, {
      data: { ...DEFAULT_ERROR_DATA, error }
    });
  }

  public handleRequest(observable: Observable<any>): Subscription {
    return observable.subscribe(() => this.openSuccessDialog(), (error) => this.openErrorDialog(error));
  }
}
