import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface IBasicDialogData {
  title: string;
  content: string;
  firstButtonCaption?: string;
  secondButtonCaption?: string;
}

@Component({
  selector: 'app-basic-dialog',
  templateUrl: './basic-dialog.component.html',
  styleUrls: ['./basic-dialog.component.scss']
})
export class BasicDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

}
