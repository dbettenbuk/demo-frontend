import { Component } from '@angular/core';
import { AuthService } from 'src/app/authentication/services/auth.service';
import { IUser } from '../../util/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  public get user(): IUser {
    return this.authService.currentUser;
  }

  constructor(private authService: AuthService) { }

  onClickLogout() {
    this.authService.logout().subscribe(() => {
      window.location.reload();
    });
  }

}
