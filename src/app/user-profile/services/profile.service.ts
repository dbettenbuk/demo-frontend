import { Injectable } from '@angular/core';
import { ApiClient } from 'src/app/shared/util/api-client';
import { IUser } from 'src/app/shared/util/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private apiClient: ApiClient) { }

  public modifyProfile(user: IUser): Observable<void> {
    return this.apiClient.post<void>('user/profile', user);
  }

  public changePassword(currentPassword: string, newPassword: string): Observable<void> {
    return this.apiClient.post('user/profile/password', { currentPassword, newPassword });
  }
}
