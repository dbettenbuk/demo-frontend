import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/authentication/services/auth.service';
import { IUser } from 'src/app/shared/util/user';
import { ProfileService } from '../../services/profile.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogService } from 'src/app/shared/services/dialog.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  userForm: FormGroup;
  passwordForm: FormGroup;

  public get user(): IUser {
    return this.authService.currentUser;
  }

  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
    private dialogService: DialogService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      email: [this.user.email, [Validators.email, Validators.required]],
      city: [this.user.address ? this.user.address.city : ''],
      streetAddress: [this.user.address ? this.user.address.streetAddress : ''],
    });

    this.passwordForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
    });
  }

  onSubmitProfile() {
    const data: IUser = {
      email: this.userForm.controls.email.value,
      address: {
        city: this.userForm.controls.city.value,
        streetAddress: this.userForm.controls.streetAddress.value,
      }
    };

    this.dialogService.handleRequest(this.profileService.modifyProfile(data));
  }

  onSubmitPassword() {
    this.dialogService.handleRequest(
      this.profileService.changePassword(
        this.passwordForm.controls.currentPassword.value,
        this.passwordForm.controls.newPassword.value
      )
    );
  }
}
