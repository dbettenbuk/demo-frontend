import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPageComponent } from './components/user-page/user-page.component';
import { SharedModule } from '../shared/shared.module';
import { EditUserDialogComponent } from './components/edit-user-dialog/edit-user-dialog.component';
import { AddressPageComponent } from './components/address-page/address-page.component';
import { EditAddressDialogComponent } from './components/edit-address-dialog/edit-address-dialog.component';
import { UserDetailsPageComponent } from './components/user-details-page/user-details-page.component';

@NgModule({
  declarations: [UserPageComponent, EditUserDialogComponent, AddressPageComponent, EditAddressDialogComponent, UserDetailsPageComponent],
  imports: [
    CommonModule,
    SharedModule,
  ],
  entryComponents: [
    EditUserDialogComponent,
    EditAddressDialogComponent,
  ]
})
export class AdminModule { }
