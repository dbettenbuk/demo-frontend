import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IAddress } from 'src/app/shared/util/address';
import { BasicDialogComponent } from 'src/app/shared/components/basic-dialog/basic-dialog.component';
import { MatDialog } from '@angular/material';
import { AddressService } from '../../services/address.service';
import { EditAddressDialogComponent } from '../edit-address-dialog/edit-address-dialog.component';
import { AuthService } from 'src/app/authentication/services/auth.service';

@Component({
  selector: 'app-address-page',
  templateUrl: './address-page.component.html',
  styleUrls: ['./address-page.component.scss']
})
export class AddressPageComponent implements OnInit {

  addresses$: Observable<IAddress[]>;

  tableColumns = ['id', 'city', 'address'];

  public get showActions(): boolean {
    return this.authService.isAdmin();
  }

  constructor(
    private addressService: AddressService,
    private authService: AuthService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.addresses$ = this.addressService.getAddresses();

    if (this.showActions) {
            this.tableColumns.push('actions');
    }
  }

  onClickModify(address: IAddress) {
    this.dialog.open(EditAddressDialogComponent, {
      data: { address },
    }).afterClosed().subscribe((modifiedUser) => {
      if (modifiedUser) {
        this.addressService.modifyAddress(modifiedUser).subscribe(() => {
          this.refreshUsers();
          this.openSuccessDialog();
        }, (error) => {
          this.openErrorDialog(error);
        });
      }
    });
  }

  onClickDelete(address: IAddress) {
    this.dialog.open(BasicDialogComponent, {
      data: {
        title: 'Confirm deletion',
        content: 'Are you sure?',
        firstButtonCaption: 'Cancel',
        secondButtonCaption: 'Yes',
      }
    }).afterClosed().subscribe((confirm) => {
      if (confirm === true) {
        this.addressService.deleteAddress(address.id)
          .subscribe(() => {
            this.refreshUsers();
            this.openSuccessDialog();
          }, (error) => {
            this.openErrorDialog(error);
          });
      }
    });
  }

  onClickAdd() {
    this.dialog.open(EditAddressDialogComponent, {
      data: {
        isNewAddress: true,
      }
    }).afterClosed().subscribe((newUser) => {
      if (newUser) {
        this.addressService.addAddress(newUser).subscribe(() => {
          this.refreshUsers();
          this.openSuccessDialog();
        }, (error) => {
          this.openErrorDialog(error);
        });
      }
    });
  }

  private openSuccessDialog() {
    this.dialog.open(BasicDialogComponent, {
      data: {
        title: 'Success',
        content: 'Operation successful',
        secondButtonCaption: 'Ok',
      }
    });
  }

  private openErrorDialog(error) {
    this.dialog.open(BasicDialogComponent, {
      data: {
        title: 'Error',
        content: error,
        secondButtonCaption: 'Ok',
      }
    });
  }

  private refreshUsers() {
    this.addresses$ = this.addressService.getAddresses();
  }

}
