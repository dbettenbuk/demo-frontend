import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/shared/util/user';
import { UserService } from '../../services/user.service';
import { MatDialog } from '@angular/material';
import { BasicDialogComponent } from 'src/app/shared/components/basic-dialog/basic-dialog.component';
import { EditUserDialogComponent } from '../edit-user-dialog/edit-user-dialog.component';
import { DialogService } from 'src/app/shared/services/dialog.service';
import handleRefresh from 'src/app/shared/util/handle-refresh';
import { AuthService } from 'src/app/authentication/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  users$: Observable<IUser[]>;

  tableColumns: string[];

  public get showActions(): boolean {
    return this.authService.isAdmin();
  }

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private matDialog: MatDialog,
    private dialogService: DialogService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.users$ = this.userService.getUsers();
    this.tableColumns = ['id', 'email', 'city', 'address'];

    if (this.showActions) {
      this.tableColumns.push('actions');
    }
  }

  onClickModify(user: IUser) {
    this.matDialog.open(EditUserDialogComponent, {
      data: { user },
    }).afterClosed().subscribe((modifiedUser) => {
      if (modifiedUser) {
        this.dialogService.handleRequest(
          this.userService.modifyUser(modifiedUser).pipe(handleRefresh(() => this.refreshUsers()))
        );
      }
    });
  }

  onClickDelete(user: IUser) {
    this.matDialog.open(BasicDialogComponent, {
      data: {
        title: 'Confirm deletion',
        content: 'Are you sure?',
        firstButtonCaption: 'Cancel',
        secondButtonCaption: 'Yes',
      }
    }).afterClosed().subscribe((confirm) => {
      if (confirm === true) {
        this.dialogService.handleRequest(
          this.userService.deleteUser(user.id).pipe(handleRefresh(() => this.refreshUsers()))
        );
      }
    });
  }

  onClickAdd() {
    this.matDialog.open(EditUserDialogComponent, {
      data: {
        isNewUser: true,
      }
    }).afterClosed().subscribe((newUser) => {
      if (newUser) {
        this.dialogService.handleRequest(
          this.userService.addUser(newUser).pipe(handleRefresh(() => this.refreshUsers()))
        );
      }
    });
  }

  showDetails(id: string) {
    this.router.navigateByUrl(`users/${id}`);
  }

  isSelf(user: IUser) {
    return user.id === this.authService.currentUser.id;
  }

  private refreshUsers() {
    this.users$ = this.userService.getUsers();
  }
}
