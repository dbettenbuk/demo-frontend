import { Component, OnInit, Inject } from '@angular/core';
import { IAddress } from 'src/app/shared/util/address';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-edit-address-dialog',
  templateUrl: './edit-address-dialog.component.html',
  styleUrls: ['./edit-address-dialog.component.scss']
})
export class EditAddressDialogComponent implements OnInit {

  addressForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { address?: IAddress, isNewAddress?: boolean },
    private dialogRef: MatDialogRef<EditAddressDialogComponent>,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.addressForm = this.formBuilder.group({
      city: [this.data.address ? this.data.address.city : ''],
      streetAddress: [this.data.address ? this.data.address.streetAddress : ''],
    });
  }

  onSubmit() {
    this.dialogRef.close({
      id: this.data.isNewAddress ? null : this.data.address.id,
      city: this.addressForm.controls.city.value,
      streetAddress: this.addressForm.controls.streetAddress.value,
    });
  }

}
