import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IUser } from 'src/app/shared/util/user';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})
export class EditUserDialogComponent implements OnInit {

  userForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { user?: IUser, isNewUser?: boolean },
    private dialogRef: MatDialogRef<EditUserDialogComponent>,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    const hasUser = this.data.user;
    this.userForm = this.formBuilder.group({
      email: [hasUser ? this.data.user.email : '', Validators.required],
      city: [hasUser && this.data.user.address ? this.data.user.address.city : ''],
      streetAddress: [hasUser && this.data.user.address ? this.data.user.address.streetAddress : ''],
    });

    if (this.data.isNewUser) {
      this.userForm.addControl('password', new FormControl('', [Validators.required]));
    }
  }

  onSubmit() {
    this.dialogRef.close({
      id: this.data.isNewUser ? null : this.data.user.id,
      email: this.userForm.controls.email.value,
      password: this.data.isNewUser ? this.userForm.controls.password.value : null,
      address: this.hasAddress() ? {
        city: this.userForm.controls.city.value,
        streetAddress: this.userForm.controls.streetAddress.value,
      } : null,
    });
  }

  private hasAddress(): boolean {
    return this.userForm.controls.city.value !== '' && this.userForm.controls.streetAddress.value !== '';
  }
}
