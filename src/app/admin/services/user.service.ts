import { Injectable } from '@angular/core';
import { ApiClient } from 'src/app/shared/util/api-client';
import { IUser } from 'src/app/shared/util/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private apiClient: ApiClient) { }

  public addUser(user: IUser): Observable<void> {
    return this.apiClient.post('user', user);
  }

  public modifyUser(user: IUser): Observable<void> {
    return this.apiClient.post(`user/${user.id}`, user);
  }

  public deleteUser(id: string): Observable<void> {
    return this.apiClient.delete(`user/${id}`);
  }

  public getUsers(): Observable<IUser[]> {
    return this.apiClient.get('user');
  }

  public getUserById(id: string): Observable<IUser> {
    return this.apiClient.get(`user/${id}`);
  }
}
