import { Injectable } from '@angular/core';
import { ApiClient } from 'src/app/shared/util/api-client';
import { IAddress } from 'src/app/shared/util/address';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private apiClient: ApiClient) { }

  public addAddress(address: IAddress): Observable<void> {
    return this.apiClient.post('user/address', address);
  }

  public modifyAddress(address: IAddress): Observable<void> {
    return this.apiClient.post(`user/address/${address.id}`, address);
  }

  public deleteAddress(id: string): Observable<void> {
    return this.apiClient.delete(`user/address/${id}`);
  }

  public getAddresses(): Observable<IAddress[]> {
    return this.apiClient.get('user/address');
  }
}
