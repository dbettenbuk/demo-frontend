import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { CredentialFormComponent } from './components/credential-form/credential-form.component';
import { SharedModule } from '../shared/shared.module';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { TokenInterceptor } from './util/token-interceptor';
import { ApiClient } from '../shared/util/api-client';
import { AuthService } from './services/auth.service';

@NgModule({
  declarations: [LoginPageComponent, RegisterPageComponent, CredentialFormComponent],
  imports: [
    CommonModule,
    SharedModule,
  ],
  providers: [
    ApiClient,
    AuthService,
    HttpClient,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class AuthenticationModule { }
