import { Component, Output, Input, OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

export interface ICredentials {
  email: string;
  password: string;
}

@Component({
  selector: 'app-credential-form',
  templateUrl: './credential-form.component.html',
  styleUrls: ['./credential-form.component.scss']
})
export class CredentialFormComponent implements OnInit {

  formGroup: FormGroup;

  @Output()
  submitForm = new EventEmitter();

  @Input()
  title: string;

  @Input()
  error?: string;

  @Input()
  buttonCaption: string;

  ngOnInit() {
    this.formGroup = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  onSubmit() {
    this.submitForm.emit({
      email: this.formGroup.controls.email.value,
      password: this.formGroup.controls.password.value,
    });
  }
}
