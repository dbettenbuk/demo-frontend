import { Component } from '@angular/core';
import { ICredentials } from '../credential-form/credential-form.component';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent {

  error: string;

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  onSubmit(data: ICredentials) {
    this.authService.login(data.email, data.password)
      .subscribe(() => {
        this.router.navigate(['']);
      }, (error) => {
        if (error) {
          this.error = 'Invalid E-mail or password';
        }
      });
  }
}
