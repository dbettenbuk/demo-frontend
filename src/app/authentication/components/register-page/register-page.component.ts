import { Component } from '@angular/core';
import { ICredentials } from '../credential-form/credential-form.component';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent {

  error: string;

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  onSubmit(data: ICredentials) {
    this.authService.register(data.email, data.password)
      .subscribe(() => {
        this.router.navigate(['login']);
      }, (error) => {
        if (error) {
          this.error = 'This E-mail already exists';
        }
      });
  }
}
