import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiClient } from 'src/app/shared/util/api-client';
import { map } from 'rxjs/operators';
import { IUser } from 'src/app/shared/util/user';
import { ILoginResponse } from '../util/login-response';
import { UserRoleType } from 'src/app/shared/util/user-role-type';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: IUser;

  public get currentUser(): IUser {
    return this.user;
  }

  constructor(private apiClient: ApiClient) { }

  public login(email: string, password: string): Observable<ILoginResponse> {
    return this.apiClient.post<ILoginResponse>('auth/login', { email, password })
      .pipe(
        map((response) => {
          localStorage.setItem('token', response.token);
          this.user = response.user;
          return response;
        })
      );
  }

  public register(email: string, password: string): Observable<string> {
    return this.apiClient.post<string>('auth/register', { email, password });
  }

  public logout(): Observable<void> {
    return this.apiClient.get<void>('auth/logout');
  }

  public authenticate(): Observable<IUser> {
    return this.apiClient.get<IUser>('auth')
      .pipe(
        map((user) => {
          this.user = user;
          return user;
        })
      );
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public isAdmin(): boolean {
    return !!this.user.roles.find((role) => role.type === UserRoleType.ADMIN);
  }
}
