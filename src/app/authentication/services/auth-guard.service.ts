import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  public async canActivate(): Promise<boolean> {
    try {
      await this.authService.authenticate().toPromise();
      return true;
    } catch {
      this.router.navigate(['login']);
      return false;
    }
  }
}
