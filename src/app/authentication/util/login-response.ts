import { IUser } from 'src/app/shared/util/user';

export interface ILoginResponse {
  user: IUser;
  token: string;
}
